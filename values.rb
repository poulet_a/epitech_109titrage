#!/usr/bin/env ruby
#encoding: utf-8

ROUND = 1
INCR = 10**(-ROUND).to_f

def data_convert values
  data = []
  values[:x].size.times do |i|
    data << [values[:x][i], values[:y][i]]
  end
  return data
end

def map_convert values
  data = []
  values[:x].size.times do |i|
    data << values[:y][i]
  end
  return data
end

def data_get_next data, id
  i = id + 1
  while data[i] and data[i][1] == -1
    i += 1
  end
  return i if data[i]
  return nil if id == -1
  return id if data_get_next data, -1
end

def convert_regular_data data
  id_prev = 0
  id_next = 0
  (data.size).times do |id|
    id_next = data_get_next(data, id)

    p1 = EpiMath::Point.new_a(data[id_prev])
    p2 = EpiMath::Point.new_a(data[id_next])
    ra = ((id_prev - id) / (id_next - id_prev).to_f).round(ROUND)
    p = p1 + ((p2 - p1) * ra)

    if data[id][1] == -1
      if p
        #puts "SUCCES #{id}"
        data[id] = [p.x.round(ROUND), p.y.round(ROUND)]
      else
        puts "FAIL #{id}"
        #puts p1
        #puts p2
        data[id][1] = data[id_prev][1]
      end
    end

    id_prev = id if data[id][1] != -1
    #puts "#{id_prev}=#{data[id_prev][1]} ; #{id}=#{data[id]} ; #{id_next}=#{data[id_next]}"
  end
  return data
end

def get_values
  values = {x: [], y: []}
  while (str = STDIN.gets.chomp and str != "FIN")
    split = str.split(";")
    values[:x] << split[0].to_f.round(ROUND)
    values[:y] << split[1].to_f.round(ROUND)
  end
  return values
end

def get_regular_values values
  data = []
  i = values[:x].min
  id_last = 0
  while i <= values[:x].max
    id = values[:x].index(i)
    if id
      data << [i, values[:y][id]]
      id_last = id
    else
      #puts "#{i} not found"
      data << [i, -1]
    end
    #puts data[-1]
    i = (i + INCR).round(ROUND)
  end

  return convert_regular_data(data)
end
