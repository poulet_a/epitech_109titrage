.\" Manpage for 109titrage.
.\" Contact poulet_a@epitech.eu and broggi_t@epitech.eu in to correct errors or typos.
.TH 109TITRAGE "25" "April 2014" "1.0" "109titrage man page"
.SH NAME
.PP
109titrage \- calulate the equivalent volume

.SH SYNOPSIS
.PP
\fB./109titrage\fR [\fPPARAMS\fR]...

.SH DESCRIPTION
.PP
\fB./109titrage\fR is a program that calcul the equivalent volume of a chimic reaction. You can calculate it using two methods : the derivate method or the tangents method. Without parameter the derivate method is used. Using the tangents method, the given parameter is the angle used for the tangents. It reads the data on the standard input.

.SH OPTIONS
.TP
\fB--nodisplay\fR
The image is no displayed at the end.
.TP
\fB-o\fR, \fB--out\fR, \fB--output\fR
Change the output format. The available formats are jpg, png and pdf. png is default value
.TP
\fB-r\fR, \fB--resolution\fR
Change the resolution of the image. default value is 1200x900
.TP
\fB-t\Fr, \fB--title\fR
Change the title of the window

.SH BONUS
.PP
There is a manpage, you can change the output format of the image, his resolution and his title. You can also no display the image, if you just want to have the equivalent volume.

.SH EXAMPLES
.TP
\fB./109titrage < sample.txt\fR
Read the data in sample.txt and calulate the equivalent volume using the derivate method
.TP
\fB./109titrage 60 < sample.txt\fR
Read the data in sample.txt and calculate the equivalent volume using the tangents method with an angle of 60 degrees
.TP
\fB./109titrage --nodisplay < sample.txt\fR
Read the data in sample.txt and calculate the equivalent volume using the derivate method, but without any display
.TP
\fB./109titrage 60 -o jpg -t "graph" -r 1920x1080 < sample.txt\fR
Read the data in sample.txt and calculate the equivalent volume using the tangents method with an angle of 60 degrees. The window will be name "graph" and have a resolution of 1920x1080, in jpg format.

.SH SEE ALSO
No related manpage.

.SH REPORTING BUGS
No known bugs.
.br
Report ./109titrage bugs to arthur.poulet@epitech.eu and thibaut.broggi@epitech.eu

.SH AUTHOR
poulet_a, broggi_t
